import numpy as np
import pickle
import glob
import matplotlib.pyplot as plt
import cv2
import pandas as pd

train_data = []
label_data = []

for x in glob.glob("C:\\Users\\Richard\\Desktop\\Dataset\\DatasetCNN\\nonrambu\\*.jpg"):
	train_data.append((cv2.cvtColor(cv2.imread(x),cv2.COLOR_BGR2RGB)))
	label_data.append(0)

for x in glob.glob("C:\\Users\\Richard\\Desktop\\Dataset\\DatasetCNN\\rambu\\*.jpg"):
	train_data.append((cv2.cvtColor(cv2.imread(x),cv2.COLOR_BGR2RGB)))
	label_data.append(1)

train_data_rambu = {"train_data" : np.array(train_data), "label_data" : np.array(label_data)}
print(train_data_rambu["train_data"].shape)
output = open('C:\\Users\\Richard\\Desktop\\DatasetCNN.p','wb')
pickle.dump(train_data_rambu,output)
output.close()

csvWrite = open('C:\\Users\\Richard\\Desktop\\DatasetCNN.csv','w')
columnTitleRow = "ClassID,RambuOrNot\n"
csvWrite.write(columnTitleRow)
csvWrite.write("0,Non Rambu\n")
csvWrite.write("1,Rambu\n")
csvWrite.close()